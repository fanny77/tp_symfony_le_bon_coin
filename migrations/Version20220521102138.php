<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220521102138 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annonce ADD id_user_id INT DEFAULT NULL, ADD id_category_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E579F37AE5 FOREIGN KEY (id_user_id) REFERENCES `user` (id)');
        $this->addSql('ALTER TABLE annonce ADD CONSTRAINT FK_F65593E5A545015 FOREIGN KEY (id_category_id) REFERENCES category (id)');
        $this->addSql('CREATE INDEX IDX_F65593E579F37AE5 ON annonce (id_user_id)');
        $this->addSql('CREATE INDEX IDX_F65593E5A545015 ON annonce (id_category_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E579F37AE5');
        $this->addSql('ALTER TABLE annonce DROP FOREIGN KEY FK_F65593E5A545015');
        $this->addSql('DROP INDEX IDX_F65593E579F37AE5 ON annonce');
        $this->addSql('DROP INDEX IDX_F65593E5A545015 ON annonce');
        $this->addSql('ALTER TABLE annonce DROP id_user_id, DROP id_category_id');
    }
}
