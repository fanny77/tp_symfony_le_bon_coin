<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=AnnonceRepository::class)
 */
class Annonce
{
	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue
	 * @ORM\Column(type="integer")
	 */
	private $id;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\Length(max = 255, maxMessage = "titre trop long")
	 * @Assert\Length(min = 2, minMessage = "titre trop court")
	 * @Assert\NotBlank(message = "Ce champs ne peut être vide")
	 */
	private $title;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\Length(max = 255, maxMessage = "description trop long")
	 * @Assert\Length(min = 2, minMessage = "description trop courte")
	 * @Assert\NotBlank(message = "Ce champs ne peut être vide")
	 */
	private $description;

	/**
	 * @ORM\Column(type="float")
	 * @Assert\NotBlank(message = "Ce champs ne peut être vide")
	 */
	private $price;

	/**
	 * @ORM\Column(type="datetime")
	 */
	private $created_at;

	/**
	 * @ORM\Column(type="datetime", nullable=true)
	 */
	private $updated_at;

	/**
	 * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="annonces")
	 */
	private $category;

	/**
	 * @ORM\ManyToOne(targetEntity=User::class, inversedBy="annonces")
	 */
	private $user;

	public function getId(): ?int
	{
		return $this->id;
	}

	public function getTitle(): ?string
	{
		return $this->title;
	}

	public function setTitle(string $title): self
	{
		$this->title = $title;

		return $this;
	}

	public function getDescription(): ?string
	{
		return $this->description;
	}

	public function setDescription(string $description): self
	{
		$this->description = $description;

		return $this;
	}

	public function getPrice(): ?float
	{
		return $this->price;
	}

	public function setPrice(float $price): self
	{
		$this->price = $price;

		return $this;
	}

	public function getCreated_At(): ?\DateTimeInterface
	{
		return $this->created_at;
	}

	public function setCreatedAt(\DateTimeInterface $created_at): self
	{
		$this->created_at = $created_at;

		return $this;
	}

	public function getUpdatedAt(): ?\DateTimeInterface
	{
		return $this->updated_at;
	}

	public function setUpdatedAt(\DateTimeInterface $updated_at): self
	{
		$this->updated_at = $updated_at;

		return $this;
	}

	public function getCategory(): ?Category
	{
		return $this->category;
	}

	public function setCategory(?Category $category): self
	{
		$this->category = $category;

		return $this;
	}

	public function getUser(): ?User
	{
		return $this->user;
	}

	public function setUser(?User $user): self
	{
		$this->user = $user;

		return $this;
	}
}
