<?php

namespace App\Controller;

use App\Repository\AnnonceRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FrontController extends AbstractController
{
	/**
	 * @Route("/", name="home")
	 */
	public function index(AnnonceRepository $annonce_repo): Response
	{
		$annonces = $annonce_repo->findAll();

		return $this->render('/index.html.twig', [
			'annonces' => $annonces,
		]);
	}
}
