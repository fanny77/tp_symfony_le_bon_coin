<?php

namespace App\Controller;

use App\Entity\Annonce;
use App\Form\AnnonceType;
use App\Repository\AnnonceRepository;
use App\Repository\UserRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AnnonceController extends AbstractController
{
	/**
	 * @Route("/detail/{id}", name="details", requirements={"id"="\d+"})
	 */
	public function details(AnnonceRepository $annonce_repo, $id): Response
	{
		$annonce = $annonce_repo->find($id);

		if (!$annonce) {
			throw $this->createNotFoundException('Aucune annonce n\'a été trouvée pour ' . $id);
		}

		return $this->render('annonce/details.html.twig', [
			'annonce' => $annonce
		]);
	}

	/**
	 * @Route("/delete/{id}", name="delete", requirements={"id"="\d+"})
	 * @IsGranted("ROLE_USER")
	 */
	public function delete(AnnonceRepository $annonce_repo, $id, EntityManagerInterface $manager): Response
	{
		$annonce = $annonce_repo->find($id);

		if (!$annonce) {
			throw $this->createNotFoundException('Aucune annonce n\'a été trouvée pour ' . $id);
		}

		$manager->remove($annonce);
		$manager->flush();

		return $this->redirectToRoute('home');
	}

	/**
	 * @Route("/add", name="add")
	 * @IsGranted("ROLE_USER")
	 */
	public function add(EntityManagerInterface $manager, Request $request, UserRepository $user_repo): Response
	{
		$annonce = new Annonce();
		
		$form = $this->createForm(AnnonceType::class);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			
			$annonce = $form->getData();
			$annonce->setCreatedAt(new DateTime());
			$annonce->setUser($this->getUser());

			$manager->persist($annonce);
			$manager->flush();
			
			$this->addFlash(
				'notice',
				'Annonce ajoutée avec succès'
			);
			
			return $this->redirectToRoute('home');
		}

		return $this->render('annonce/ajouter.html.twig',[
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/update/{id}", name="update", requirements={"id"="\d+"})
	 * @IsGranted("ROLE_USER")
	 */
	public function update(AnnonceRepository $annonce_repo, $id, EntityManagerInterface $manager, Request $request): Response
	{
		$annonce = $annonce_repo->find($id);
		
		if (!$annonce) {
			throw $this->createNotFoundException('Aucune annonce n\'a été trouvée pour ' . $id);
		}
		$form = $this->createForm(AnnonceType::class, $annonce);
		$form->handleRequest($request);
		if ($form->isSubmitted() && $form->isValid()) {
			
			$annonce = $form->getData();
			$annonce->setUpdatedAt(new DateTime());

			$manager->flush();
			
			$this->addFlash(
				'notice',
				'Annonce modifiée avec succès'
			);
			
			return $this->redirectToRoute('details', [
				'id' => $id
			]);
		}

		return $this->render('annonce/modifier.html.twig',[
			'form' => $form->createView()
		]);	
	}
}