<?php 

namespace App\DataFixtures;

use DateTime;
use App\Entity\Annonce;
use App\Repository\UserRepository;
use App\Repository\CategoryRepository;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;

class AnnoncesFixtures extends Fixture  implements DependentFixtureInterface
{

	private $user_repo;
	private $category_repo;

	function __construct(UserRepository $user_repo, CategoryRepository $category_repo)
	{
		$this->user_repo = $user_repo;
		$this->category_repo = $category_repo;
	}

	public function load(ObjectManager $manager)
	{
		$annonce1 = new Annonce();
		$annonce1->setTitle("Télé") 
				->setPrice(100) 
				->setDescription("TV qui marche très bien !") 
				->setCreatedAt(new DateTime()) 
				->setCategory($this->category_repo->find(1))
				->setUser($this->user_repo->find(1));
		
		$annonce2 = new Annonce();
		$annonce2->setTitle("Vélo") 
				->setPrice(130) 
				->setDescription("VTT très bon état") 
				->setCreatedAt(new DateTime()) 
				->setCategory($this->category_repo->find(2))
				->setUser($this->user_repo->find(1));


		$annonce3 = new Annonce();
		$annonce3->setTitle("Table de salon") 
				->setPrice(55) 
				->setDescription("table en bois, très bon état") 
				->setCreatedAt(new DateTime()) 
				->setCategory($this->category_repo->find(3))
				->setUser($this->user_repo->find(1));
		
		
		$annonce4 = new Annonce();
		$annonce4->setTitle("Matelas") 
				->setPrice(50) 
				->setDescription("très confortable") 
				->setCreatedAt(new DateTime()) 
				->setCategory($this->category_repo->find(3))
				->setUser($this->user_repo->find(1));

		$annonce5 = new Annonce();
		$annonce5->setTitle("Jouet pour chien") 
				->setPrice(20) 
				->setDescription("neuf") 
				->setCreatedAt(new DateTime()) 
				->setCategory($this->category_repo->find(4))
				->setUser($this->user_repo->find(1));
						


		$manager->persist($annonce1);
		$manager->persist($annonce2);
		$manager->persist($annonce3);
		$manager->persist($annonce4);
		$manager->persist($annonce5);
		
		$manager->flush();
	}

	public function getDependencies()
	{
		return [
			UserFixtures::class,
			CategoryFixtures::class
		];
	}
}