<?php 

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture

{
	public function load(ObjectManager $manager)
	{
		$category1 = new Category();
		$category1->setTitle("Multimédia");
		
		$category2 = new Category();
		$category2->setTitle("Loisir");

		$category3 = new Category();
		$category3->setTitle("Immobilier");

		$category4 = new Category();
		$category4->setTitle("Animaux");

		$manager->persist($category1);
		$manager->persist($category2);
		$manager->persist($category3);
		$manager->persist($category4);

		$manager->flush();
	}
}