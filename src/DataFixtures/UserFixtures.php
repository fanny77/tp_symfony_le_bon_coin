<?php 

namespace App\DataFixtures;


use DateTime;
use App\Entity\User;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Bundle\FixturesBundle\Fixture;

class UserFixtures extends Fixture

{
	public function load(ObjectManager $manager)
	{

		$user1 = new User();
		$user1->setUsername("fanny");
		$user1->setPassword("motusuel");
		$user1->setCreatedAt(new DateTime());
		$user1->setRoles([]);
		
		$manager->persist($user1);
		$manager->flush();
	}


}