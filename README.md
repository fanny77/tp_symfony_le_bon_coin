# TP2 - Site Le bon coin

Évaluation module Symfony MDS MELUN : création d'une interface de gestion d'annonces en ligne 

[[_TOC_]]

## Pour commencer

### Pré-requis

- composer
- Symfony CLI
- MySQL

### Installation

Clonez le dépôt Gitlab : 
```bash 
git clone https://gitlab.com/fanny77/tp_symfony_le_bon_coin.git 
```

Placez vous dans le dossier 'tp_blog_symfony' et exécutez la commande :
```bash
 composer install
 ```

Configurez le fichier .env et créez la base de données avec la commande :
```bash
 php bin/console doctrine:database:create
 ```

Jouez les migrations : 
```bash
 php bin/console doctrine:migrations:migrate
 ```

Vous pouvez pré-remplir la base de données en jouant les fixtures :
```bash
 php bin/console doctrine:fixtures:load
 ```

## Démarrage

Toujours dans le dossier 'tp_blog_symfony' et exécutez la commande : 
```bash
symfony serve
 ```

## Fabriqué avec

* [PHP MyAdmin](https://www.phpmyadmin.net/) - Base de données
* [Symfony](https://symfony.com/) - Framework PHP (back-end)
* [Doctrine](https://www.doctrine-project.org/projects/orm.html) - ORM
* [Bootstrap](https://getbootstrap.com/) -v4.6 Framework CSS (front-end)
* [Visual Studio Code](https://code.visualstudio.com/) - Editeur de textes

## Auteurs

* **Fanny GOUBERT** _alias_ [@fanny77](https://gitlab.com/fanny77)
